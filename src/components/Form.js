import React, { Component } from "react";
import $ from 'jquery';
import store from "../store"


// Extende class component.
class Form extends Component {
   constructor() {
      super();
      // Init obj.
      this.state = {
         id: "",
         title: "",
         responsible: "",
         description: "",
         priority: "",
         errors: {},
         hasError: {
            title: false,
            responsible: false,
            description: false
         }
      };
      // Add reference this.
      this.handelInput = this.handelInput.bind(this);
      this.handelSubmit = this.handelSubmit.bind(this);
      this.handelKeyUp = this.handelKeyUp.bind(this);

      // Listen to events store.
      store.subscribe(() => {
         // Validate array task
         if ( store.getState().task.length > 1 ) {
            // Delete first element.
            store.getState().task.shift();
         }

         // Update state.
         this.setState({
            id: store.getState().task[0].id,
            title: store.getState().task[0].title,
            responsible: store.getState().task[0].responsible,
            description: store.getState().task[0].description,
            priority: store.getState().task[0].priority,
         });
      });
   }

   // Function change.
   handelInput(e) {
      const { value, name } = e.target;
      // Update state attributes.
      this.setState({
         [name]: value
      });
   };

   // Sende data values form.
   handelSubmit(e) {
      // Avoid refreshing the page.
      e.preventDefault();
      // Validate fileds.
      if ( this.validateFields() ) {
         // Validate edit task
         if ( this.state.id !== "" ) {
            // Update task from server.
            this.props.onUpdateTask(this.state);
         } else {
            // Add task in database.
            this.props.onAddTask(this.state);
         }

         // Update state.
         this.setState({
            title: "",
            responsible: "",
            description: "",
            priority: ""
         });
      }
   };

   // Validate inputs.
   validateFields() {
      // Get attributes of the state.
      const { hasError, title, responsible, description } = this.state;
      let errors = {};
      let formIsValid = true;

      if ( !title ) {
         // Update flasg
         formIsValid = false;
         // Add text.
         errors["title"] = "*Please enter your title.";
         // Add class error.
         hasError.title = true;
      }

      if ( !responsible ) {
         // Update flasg
         formIsValid = false;
         // Add text.
         errors["responsible"] = "*Please enter your responsible.";
         // Add class error.
         hasError.responsible = true;
      }

      if ( !description ) {
         // Update flasg
         formIsValid = false;
         // Add text.
         errors["description"] = "*Please enter your description.";
         // Add class error.
         hasError.description = true;
      }

      // Update state.
      this.setState({
        errors: errors,
        hasError: hasError
      });

      return formIsValid;
   }

   // Show error class.
   showError(error) {
      // Validate custom input and add class error.
      return ( !error ? '' : 'error');
   }

   // Remove class error.
   handelKeyUp(e) {
      // Get curren target.
      const target = $(e.currentTarget),
      { hasError, errors } = this.state;
      // Remove class error.
      target.removeClass("error");
      // Get input name.
      let inputName = e.currentTarget.name
      // Update flag error.
      hasError[inputName] = false;
      // Hide error description.
      errors[inputName] = "";
      // Update state.
      this.setState({
         hasError: hasError,
         errors: errors
      });
   }

   render() {
      return(
         <div className="card">
            <form className="card-body" onSubmit={this.handelSubmit}>
               <div className="form-group">
                  <input className={`form-control ${this.showError(this.state.hasError.title)}`} onKeyUp={this.handelKeyUp} type="text" name="title" value={this.state.title} placeholder="Title" onChange={this.handelInput}/>
                  <div className="errorMsg">{this.state.errors.title}</div>
               </div>
               <div className="form-group">
                  <input className={`form-control ${this.showError(this.state.hasError.responsible)}`} onKeyUp={this.handelKeyUp} type="text" value={this.state.responsible} name="responsible" placeholder="Responsible" onChange={this.handelInput}/>
                  <div className="errorMsg">{this.state.errors.responsible}</div>
               </div>
               <div className="form-group">
                  <input className={`form-control ${this.showError(this.state.hasError.description)}`} onKeyUp={this.handelKeyUp} type="text" value={this.state.description} name="description" placeholder="Description" onChange={this.handelInput}/>
                  <div className="errorMsg">{this.state.errors.description}</div>
               </div>
               <div className="form-group">
               <select name="priority" className="form-control" onChange={this.handelInput}>
                 <option>low</option>
                 <option>medium</option>
                 <option>high</option>
               </select>
               </div>
               <button type="submit" className="btn btn-primary">Save</button>
            </form>
         </div>
      )
   }
}

export default Form;
