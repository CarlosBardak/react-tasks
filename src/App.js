import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
//import { todos } from "./todos.json";
import Form from "./components/Form";
import firebase from "firebase";
import { DB_CONFIG } from "./config/config.js"
import "firebase/database";
import store from "./store";
import { editTask } from "./action-creator"



class App extends Component {
   constructor(){
      // Inherit classe.
      super();
      // State reference.
      this.state = {
         tasks: [],
         taskEdit: []
      }

      // Init firebase database.
      this.app = firebase.initializeApp(DB_CONFIG);
      this.db = this.app.database().ref().child("task");
      // Add reference this.
      this.handleAddTask = this.handleAddTask.bind(this);
      this.handleUpdateTask = this.handleUpdateTask.bind(this);
   }

   // Mount firebase.
   componentDidMount() {
      // Get tasks init.
      const { tasks } = this.state;

      // Push data form server.
      this.db.on("child_added", snap => {
         //
         let task = snap.val().task;
         // Add id of the data base.
         task.id = snap.key;
         // Insert data.
         tasks.push(task);
         // Update state.
         this.setState({tasks});
      });

      // Remove data form server.
      this.db.on("child_removed", snap => {
         // Loop array task.
         for ( let i = 0; i < tasks.length; ++i ) {
            // Remove task list.
            if ( tasks[i].id === snap.key) {
               // Remove task in array
               tasks.splice(i, 1)
            }
         }

         // Update state task.
         this.setState({tasks});
      });

      // Remove data form server.
      this.db.on("child_changed", snap => {
         // Get task list.
         const { tasks } = this.state;

         //
         for (var i = 0; i < tasks.length; ++i) {
            // Validate task.
            if ( tasks[i].id ===  snap.val().task.id ) {
               // Update task selected.
               tasks[i] = snap.val().task
            }
         };

         // Update state.
         this.setState({
            tasks: tasks
         })
      });
   }


   // Add task.
   handleAddTask(task) {
      /*this.setState({
         tasks: [...this.state.tasks, task]
      });*/
      this.db.push().set({task: task});
   };

   // Remove card task.
   delteTask(id) {
      // Remove task.
      /*this.setState({
         tasks: this.state.tasks.filter((e, i) => {
            return i !== id
         })
      });*/
      // Delete task on database.
      this.db.child(id).remove();
   };

   // Update task on firebase.
   handleUpdateTask(task) {
      //console.log(task)
      // Update task from server firebase.
      this.app.database().ref('task/' + task.id + "/task").set({
         id: task.id,
         description: task.description,
         title: task.title,
         priority: task.priority,
         responsible: task.responsible
      });
   };

   // Edit task.
   editTask(id) {
      // Get attribute.
      const { taskEdit } = this.state;
      // Remove task.
      if ( taskEdit.length > 0 ) {
         // Update state.
         this.setState({
            taskEdit: taskEdit.shift()
         });
      }

      // Get task edit.
      for (var i = 0; i < this.state.tasks.length; i++) {
         // code...
         if ( this.state.tasks[i].id === id ) {
            // Update task edit.
            taskEdit.push(this.state.tasks[i]);
         }
      }

      // Update state.
      this.setState({
         taskEdit: taskEdit
      });

      // Dispach events edit task.
      store.dispatch(editTask(taskEdit));
   };

   render() {
      // Show task item.
      const tasks = this.state.tasks.map((task, i) => {
         return(
            <div className="cold-md-4 mr-4 ml-4" key={i}>
               <div className="card mt-4">
                  <div className="card-header">
                     <h3>{task.title}</h3>
                     <span className="badge badge-pill badge-danger ml-2">{task.priority}</span>
                  </div>
                  <div className="card-body">
                     <p>{task.description}</p>
                     <p><mark>{task.responsible}</mark></p>
                  </div>
                  <div className="card-footer">
                     <button className="btn btn-danger mr-2" onClick={this.delteTask.bind(this, task.id)}>Delete</button>
                     <button className="btn btn-info" onClick={this.editTask.bind(this, task.id)}>Edit</button>
                  </div>
               </div>
            </div>
         )
      })

      return (
         <div className="App">

            <nav className="navbar navbar-dark bg-dark">
               <a className="navbar-brand" href="/">
                  Tasks
                  <span className="badge badge-pill badge-light ml-2">
                    {this.state.tasks.length}
                  </span>
               </a>
            </nav>

            <div className="container">
               <div className="row mt-4">

                  <div className="col-md-4 text-center">
                     <img src={logo} className="App-logo" alt="logo" />
                     <Form onAddTask={this.handleAddTask} onUpdateTask={this.handleUpdateTask}></Form>
                  </div>

                  <div className="col-md-8">
                    <div className="row">
                      {tasks}
                    </div>
                  </div>
               </div>
            </div>
         </div>
      );
   }
}

export default App;
