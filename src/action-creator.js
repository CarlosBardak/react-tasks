const editTask = task => {
   // Return safe.
   return {
      type: "EDIT_TASK",
      task
   }
};

export { editTask };
