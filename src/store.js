import { createStore } from "redux";

const reducer = (state, action) => {
   // Validate type acction.
   if ( action.type === "EDIT_TASK" ) {
      // Return safe.
      return {
         // Create new state copy attrinutes.
         ...state,
         task: state.task.concat(action.task)
      };
   }

   // Return safe.
   return state;
};

export default createStore(reducer, { task: [] });
